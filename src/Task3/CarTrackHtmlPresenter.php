<?php

declare(strict_types=1);

namespace App\Task3;

use App\Task1\Car;
use App\Task1\Track;

class CarTrackHtmlPresenter
{
    public function present(Track $track): string
    {
        $view = '';
        foreach($track->all() as $car) {
            $view .= '<img src="'.$car->getImage().'">' . $car->getName().': '.$car->getSpeed().', '.$car->getPitStopTime();
        }

        return $view;
    }
}