<?php

declare(strict_types=1);

namespace App\Task1;

class Track
{
    private float $lapLength;
    private int $lapsNumber;
    private array $carsAtTrack;

    public function __construct(float $lapLength, int $lapsNumber)
    {
        $this->lapLength = $lapLength;
        $this->lapsNumber = $lapsNumber;
    }

    public function getLapLength(): float
    {
        return $this->lapLength;
    }

    public function getLapsNumber(): int
    {
        return $this->lapsNumber;
    }

    public function add(Car $car): void
    {
        $this->carsAtTrack[] = $car;
    }

    public function all(): array
    {
        return $this->carsAtTrack;
    }

    public function run(): Car
    {
        $result = [];

        $trackLength = $this->lapLength * $this->lapsNumber;

        foreach($this->carsAtTrack as $car) {
            $kmForOneFuelTank = ($car->getFuelTankVolume() / $car->getFuelConsumption()) * 100;
            $countOfPitStops = floor($trackLength / $kmForOneFuelTank);
            $allPitStopsTime = $countOfPitStops * $car->getPitStopTime();
            $timeForAllTrack = ($trackLength / $car->getSpeed()) * 3600;
            $resultTime = $allPitStopsTime + $timeForAllTrack;

            if(!key_exists('time', $result) || $result['time'] > $resultTime) {
                $result['car'] = $car;
                $result['time'] = $resultTime;
            }
        }

        $this->carsAtTrack = [];

        return $result['car'];
    }
}